unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Printers;

type
  TVolume_Wood_Converter = class(TForm)
    ComboBox1: TComboBox;
    Label1: TLabel;
    Bevel2: TBevel;
    ComboBox3: TComboBox;
    PrintDialog1: TPrintDialog;
    PrinterSetupDialog1: TPrinterSetupDialog;
    Label2: TLabel;
    Button3: TButton;
    Label3: TLabel;
    Label4: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Volume_Wood_Converter: TVolume_Wood_Converter;

implementation

{$R *.DFM}

Function Volume(s_d, s_l : string): string;

var
  d, l : integer;
  v : string;

begin
  d := StrToInt(s_d);
  l := StrToInt(s_l);

  if l < 380 then
    case d of
    11 : v := '048';    12 : v := '048';
    13 : v := '066';    14 : v := '066';
    15 : v := '087';    16 : v := '087';
    17 : v := '110';    18 : v := '110';
    19 : v := '131';    20 : v := '131';
    21 : v := '164';    22 : v := '164';
    23 : v := '195';    24 : v := '195';
    25 : v := '230';    26 : v := '230';
    27 : v := '260';    28 : v := '260';
    29 : v := '300';    30 : v := '300';
    31 : v := '350';    32 : v := '350';
    33 : v := '390';    34 : v := '390';
    35 : v := '440';    36 : v := '440';
    else v := '000';
    end;

  if (380 <= l) and (l < 390) then
    case d of
    11 : v := '042';    12 : v := '050';
    13 : v := '058';    14 : v := '068';
    15 : v := '090';    16 : v := '090';
    17 : v := '113';    18 : v := '113';
    19 : v := '139';    20 : v := '139';
    21 : v := '170';    22 : v := '170';
    23 : v := '200';    24 : v := '200';
    25 : v := '240';    26 : v := '240';
    27 : v := '270';    28 : v := '270';
    29 : v := '310';    30 : v := '310';
    31 : v := '360';    32 : v := '360';
    33 : v := '410';    34 : v := '410';
    35 : v := '460';    36 : v := '460';
    else v := '000';
    end;

  if (390 <= l) and (l < 405) then
    case d of
    11 : v := '043';    12 : v := '051';
    13 : v := '060';    14 : v := '070';
    15 : v := '092';    16 : v := '092';
    17 : v := '117';    18 : v := '117';
    19 : v := '143';    20 : v := '143';
    21 : v := '173';    22 : v := '173';
    23 : v := '200';    24 : v := '200';
    25 : v := '240';    26 : v := '240';
    27 : v := '280';    28 : v := '280';
    29 : v := '320';    30 : v := '320';
    31 : v := '370';    32 : v := '370';
    33 : v := '420';    34 : v := '420';
    35 : v := '470';    36 : v := '470';
    else v := '000';
    end;

  if (405 <= l) and (l < 480) then
    case d of
    11 : v := '045';    12 : v := '053';
    13 : v := '062';    14 : v := '073';
    15 : v := '095';    16 : v := '095';
    17 : v := '120';    18 : v := '120';
    19 : v := '147';    20 : v := '147';
    21 : v := '178';    22 : v := '178';
    23 : v := '210';    24 : v := '210';
    25 : v := '250';    26 : v := '250';
    27 : v := '290';    28 : v := '290';
    29 : v := '330';    30 : v := '330';
    31 : v := '380';    32 : v := '380';
    33 : v := '430';    34 : v := '430';
    35 : v := '480';    36 : v := '480';
    else v := '000';
    end;

  if (480 <= l) and (l < 490) then
    case d of
    11 : v := '059';    12 : v := '069';
    13 : v := '082';    14 : v := '092';
    15 : v := '118';    16 : v := '118';
    17 : v := '150';    18 : v := '150';
    19 : v := '183';    20 : v := '183';
    21 : v := '220';    22 : v := '220';
    23 : v := '260';    24 : v := '260';
    25 : v := '310';    26 : v := '310';
    27 : v := '350';    28 : v := '350';
    29 : v := '410';    30 : v := '410';
    31 : v := '460';    32 : v := '460';
    33 : v := '520';    34 : v := '520';
    35 : v := '580';    36 : v := '580';
    else v := '000';
    end;

  if (490 <= l) and (l < 505) then
    case d of
    11 : v := '060';    12 : v := '071';
    13 : v := '083';    14 : v := '094';
    15 : v := '121';    16 : v := '121';
    17 : v := '152';    18 : v := '152';
    19 : v := '187';    20 : v := '187';
    21 : v := '220';    22 : v := '220';
    23 : v := '260';    24 : v := '260';
    25 : v := '310';    26 : v := '310';
    27 : v := '360';    28 : v := '360';
    29 : v := '410';    30 : v := '410';
    31 : v := '470';    32 : v := '470';
    33 : v := '530';    34 : v := '530';
    35 : v := '590';    36 : v := '590';
    else v := '000';
    end;

  if (505 <= l) and (l < 580) then
    case d of
    11 : v := '062';    12 : v := '073';
    13 : v := '085';    14 : v := '097';
    15 : v := '124';    16 : v := '124';
    17 : v := '156';    18 : v := '156';
    19 : v := '190';    20 : v := '190';
    21 : v := '230';    22 : v := '230';
    23 : v := '270';    24 : v := '270';
    25 : v := '320';    26 : v := '320';
    27 : v := '370';    28 : v := '370';
    29 : v := '420';    30 : v := '420';
    31 : v := '480';    32 : v := '480';
    33 : v := '540';    34 : v := '540';
    35 : v := '600';    36 : v := '600';
    else v := '000';
    end;

  if (580 <= l) and (l < 590) then
    case d of
    11 : v := '076';    12 : v := '089';
    13 : v := '104';    14 : v := '118';
    15 : v := '149';    16 : v := '149';
    17 : v := '186';    18 : v := '186';
    19 : v := '220';    20 : v := '220';
    21 : v := '270';    22 : v := '270';
    23 : v := '320';    24 : v := '320';
    25 : v := '380';    26 : v := '380';
    27 : v := '430';    28 : v := '430';
    29 : v := '500';    30 : v := '500';
    31 : v := '570';    32 : v := '570';
    33 : v := '640';    34 : v := '640';
    35 : v := '710';    36 : v := '710';
    else v := '000';
    end;

  if (590 <= l) and (l < 605) then
    case d of
    11 : v := '078';    12 : v := '091';
    13 : v := '106';    14 : v := '120';
    15 : v := '152';    16 : v := '152';
    17 : v := '190';    18 : v := '190';
    19 : v := '230';    20 : v := '230';
    21 : v := '270';    22 : v := '270';
    23 : v := '320';    24 : v := '320';
    25 : v := '380';    26 : v := '380';
    27 : v := '440';    28 : v := '440';
    29 : v := '510';    30 : v := '510';
    31 : v := '580';    32 : v := '580';
    33 : v := '650';    34 : v := '650';
    35 : v := '720';    36 : v := '720';
    else v := '000';
    end;

  if (605 <= l) then
    case d of
    11 : v := '080';    12 : v := '093';
    13 : v := '108';    14 : v := '123';
    15 : v := '155';    16 : v := '155';
    17 : v := '194';    18 : v := '194';
    19 : v := '230';    20 : v := '230';
    21 : v := '280';    22 : v := '280';
    23 : v := '330';    24 : v := '330';
    25 : v := '390';    26 : v := '390';
    27 : v := '450';    28 : v := '450';
    29 : v := '520';    30 : v := '520';
    31 : v := '590';    32 : v := '590';
    33 : v := '660';    34 : v := '660';
    35 : v := '740';    36 : v := '740';
    else v := '000';
    end;

  Volume := '0,' + v;

end;



procedure TVolume_Wood_Converter.Button1Click(Sender: TObject);

var
  file_otchet : textfile;
  file_otchet_new : textfile;
  value_buffer : string;
  counter : integer;

  value_s_d : string;
  value_s_l : string;


begin
  assignfile(file_otchet,'Vyrez2.txt');
  if ComboBox1.ItemIndex = 0 then assignfile(file_otchet,'Vyrez2.txt');
  if ComboBox1.ItemIndex = 1 then assignfile(file_otchet,'Vyrez3.txt');
  if ComboBox1.ItemIndex = 2 then assignfile(file_otchet,'Vyrez4.txt');
  if ComboBox1.ItemIndex = 3 then assignfile(file_otchet,'Vyrez5.txt');
  if ComboBox1.ItemIndex = 4 then assignfile(file_otchet,'Vyrez6.txt');
  if ComboBox1.ItemIndex = 5 then assignfile(file_otchet,'Vyrez7.txt');
  if ComboBox1.ItemIndex = 6 then assignfile(file_otchet,'Vyrez1.txt');
  reset(file_otchet);

  assignfile(file_otchet_new,'Vyrez.txt');
  rewrite(file_otchet_new);

  readln(file_otchet, value_buffer);
  writeln(file_otchet_new, value_buffer);
  counter := 0;

  while not ( EOF(file_otchet)) do
    begin
    if counter <> 26 then counter := counter + 1 else counter := 1;
    readln(file_otchet, value_buffer);
    if counter = 12 then value_s_d := value_buffer;
    if counter = 17 then value_s_l := value_buffer;

    if counter = 19 then value_buffer := Volume(value_s_d,value_s_l);

    writeln(file_otchet_new, value_buffer);
  end;
  closefile(file_otchet);
  closefile(file_otchet_new);

end;

procedure TVolume_Wood_Converter.Button2Click(Sender: TObject);

var

  file_otchet : textfile;
  file_otchet_new : textfile;
  value_buffer : string;
  iCounter : integer;
  a_iSize : array [0..19] of Int32;
  a_iCount : array [0..19] of Int32;

  bHook : boolean;

  sTemp : string;
  iIndex : integer;

begin

  assignfile(file_otchet,'Vyrez.txt');
{
  if ComboBox2.ItemIndex = 0 then assignfile(file_otchet,'Vyrez2.txt');
  if ComboBox2.ItemIndex = 1 then assignfile(file_otchet,'Vyrez3.txt');
  if ComboBox2.ItemIndex = 2 then assignfile(file_otchet,'Vyrez4.txt');
  if ComboBox2.ItemIndex = 3 then assignfile(file_otchet,'Vyrez5.txt');
  if ComboBox2.ItemIndex = 4 then assignfile(file_otchet,'Vyrez6.txt');
  if ComboBox2.ItemIndex = 5 then assignfile(file_otchet,'Vyrez7.txt');
  if ComboBox2.ItemIndex = 6 then assignfile(file_otchet,'Vyrez1.txt');
  if ComboBox2.ItemIndex = 7 then assignfile(file_otchet,'Vyrez.txt');
}
  reset(file_otchet);

  assignfile(file_otchet_new,'Print.txt');
  rewrite(file_otchet_new);

  writeln(file_otchet_new, '��������');
  writeln(file_otchet_new, '=================================================================');
  writeln(file_otchet_new, '                                              ' + DateToStr(Date) + ' ' + TimeToStr(Time));
  writeln(file_otchet_new, '��� "������������� ���"');
  readln(file_otchet, value_buffer);
  writeln(file_otchet_new, '���� :       ' + value_buffer);

  writeln(file_otchet_new, '����� :      ' + ComboBox3.Text);

  for iCounter := 0 to 19 do a_iSize[iCounter] := 0;
  for iCounter := 0 to 19 do a_iCount[iCounter] := 0;

  while not ( EOF(file_otchet)) do begin
    bHook := False;

    readln(file_otchet, value_buffer);    //  1
    readln(file_otchet, value_buffer);    //  2

    if value_buffer = ComboBox3.Text then bHook := True;

    readln(file_otchet, value_buffer);    //  3
    readln(file_otchet, value_buffer);    //  4
    readln(file_otchet, value_buffer);    //  5
    readln(file_otchet, value_buffer);    //  6
    readln(file_otchet, value_buffer);    //  7
    readln(file_otchet, value_buffer);    //  8
    readln(file_otchet, value_buffer);    //  9
    readln(file_otchet, value_buffer);    // 10
    readln(file_otchet, value_buffer);    // 11
    readln(file_otchet, value_buffer);    // 12
    readln(file_otchet, value_buffer);    // 13
    readln(file_otchet, value_buffer);    // 14
    readln(file_otchet, value_buffer);    // 15
    readln(file_otchet, value_buffer);    // 16
    readln(file_otchet, value_buffer);    // 17
    readln(file_otchet, value_buffer);    // 18
    readln(file_otchet, value_buffer);    // 19

    if bHook then sTemp := Copy( value_buffer, 3, 3);

    readln(file_otchet, value_buffer);    // 20
    readln(file_otchet, value_buffer);    // 21

    if bHook then begin
      iIndex := StrToInt(value_buffer);
      a_iCount[iIndex] := a_iCount[iIndex] + 1;
      a_iCount[19] := a_iCount[19] + 1;
      a_iSize[iIndex] := a_iSize[iIndex] + StrToInt( sTemp );
      a_iSize[19] := a_iSize[19] + StrToInt( sTemp );
      end;

    readln(file_otchet, value_buffer);    // 22
    readln(file_otchet, value_buffer);    // 23
    readln(file_otchet, value_buffer);    // 24
    readln(file_otchet, value_buffer);    // 25
    readln(file_otchet, value_buffer);    // 26

    end;

  for iCounter := 0 to 18 do begin
    value_buffer := IntToStr(a_iSize[iCounter]);
    if Length( value_buffer ) = 1 then value_buffer := '000' + value_buffer;
    if Length( value_buffer ) = 2 then value_buffer := '00' + value_buffer;
    if Length( value_buffer ) = 3 then value_buffer := '0' + value_buffer;
    Insert(',', value_buffer, length( value_buffer ) - 2);

    write(file_otchet_new, ' ���� �');
    write(file_otchet_new, IntToStr(iCounter) + ':':3 );
    write(file_otchet_new, IntToStr(a_iCount[iCounter]) + ' ��':15 );
    writeln(file_otchet_new, value_buffer + ' �3':20 );

    end;

  value_buffer := IntToStr(a_iSize[19]);
  if Length( value_buffer ) = 1 then value_buffer := '000' + value_buffer;
  if Length( value_buffer ) = 2 then value_buffer := '00' + value_buffer;
  if Length( value_buffer ) = 3 then value_buffer := '0' + value_buffer;
  Insert(',', value_buffer, length( value_buffer ) - 2);

  writeln(file_otchet_new, '-----------------------------------------------------------------');
  write(file_otchet_new, ' ����� ');
  write(file_otchet_new, IntToStr(a_iCount[19]) + ' ��':18 );
  writeln(file_otchet_new, value_buffer + ' �3':20 );
  writeln(file_otchet_new, '-----------------------------------------------------------------');

  closefile(file_otchet);
  closefile(file_otchet_new);

  reset(file_otchet_new);

  iCounter := 0;

  with Printer do Begin
    BeginDoc;
    Canvas.Font := Label2.Font;
    while not ( EOF(file_otchet_new)) do begin
      iCounter := iCounter + 1;
      readln(file_otchet_new, value_buffer);

      Canvas.TextOut(100,iCounter*15, value_buffer );

      end;
    EndDoc;
    end;

  closefile(file_otchet_new);

end;




procedure TVolume_Wood_Converter.Button3Click(Sender: TObject);

var

  counter : integer;
  value_s_d : string;
  value_s_l : string;
  file_otchet : textfile;
  file_otchet_new : textfile;
  value_buffer : string;
  iCounter, iCounter1 : integer;
  a_iSize : array [0..19, 0..39] of Int32;
  a_iCount : array [0..19, 0..39] of Int32;

  bHook : boolean;

  iBox, iDiameter, iSize, iCount : integer;

begin

  assignfile(file_otchet,'Vyrez2.txt');
  if ComboBox1.ItemIndex = 0 then assignfile(file_otchet,'Vyrez2.txt');
  if ComboBox1.ItemIndex = 1 then assignfile(file_otchet,'Vyrez3.txt');
  if ComboBox1.ItemIndex = 2 then assignfile(file_otchet,'Vyrez4.txt');
  if ComboBox1.ItemIndex = 3 then assignfile(file_otchet,'Vyrez5.txt');
  if ComboBox1.ItemIndex = 4 then assignfile(file_otchet,'Vyrez6.txt');
  if ComboBox1.ItemIndex = 5 then assignfile(file_otchet,'Vyrez7.txt');
  if ComboBox1.ItemIndex = 6 then assignfile(file_otchet,'Vyrez1.txt');
  reset(file_otchet);

  assignfile(file_otchet_new,'Vyrez.txt');
  rewrite(file_otchet_new);

  readln(file_otchet, value_buffer);
  writeln(file_otchet_new, value_buffer);
  counter := 0;

  while not ( EOF(file_otchet)) do
    begin
    if counter <> 26 then counter := counter + 1 else counter := 1;
    readln(file_otchet, value_buffer);
    if counter = 12 then value_s_d := value_buffer;
    if counter = 17 then value_s_l := value_buffer;

    if counter = 19 then value_buffer := Volume(value_s_d,value_s_l);

    writeln(file_otchet_new, value_buffer);
  end;
  closefile(file_otchet);
  closefile(file_otchet_new);


  assignfile(file_otchet,'Vyrez.txt');

  reset(file_otchet);

  assignfile(file_otchet_new,'Print.txt');
  rewrite(file_otchet_new);

  writeln(file_otchet_new, '��������');
  writeln(file_otchet_new, '=================================================================');
  writeln(file_otchet_new, '                                              ' + DateToStr(Date) + ' ' + TimeToStr(Time));
  writeln(file_otchet_new, '��� "������������� ���"');
  readln(file_otchet, value_buffer);
  writeln(file_otchet_new, '���� :       ' + value_buffer);

  writeln(file_otchet_new, '����� :      ' + ComboBox3.Text);

  for iCounter := 0 to 19 do
    for iCounter1 := 0 to 39 do a_iSize[iCounter, iCounter1] := 0;
  for iCounter := 0 to 19 do
    for iCounter1 := 0 to 39 do a_iCount[iCounter, iCounter1] := 0;

  while not ( EOF(file_otchet)) do begin
    bHook := False;

    readln(file_otchet, value_buffer);    //  1
    readln(file_otchet, value_buffer);    //  2

    if value_buffer = ComboBox3.Text then bHook := True;

    readln(file_otchet, value_buffer);    //  3
    readln(file_otchet, value_buffer);    //  4
    readln(file_otchet, value_buffer);    //  5
    readln(file_otchet, value_buffer);    //  6
    readln(file_otchet, value_buffer);    //  7
    readln(file_otchet, value_buffer);    //  8
    readln(file_otchet, value_buffer);    //  9
    readln(file_otchet, value_buffer);    // 10
    readln(file_otchet, value_buffer);    // 11
    readln(file_otchet, value_buffer);    // 12

    if bHook then iDiameter := StrToInt( value_buffer);

    readln(file_otchet, value_buffer);    // 13
    readln(file_otchet, value_buffer);    // 14
    readln(file_otchet, value_buffer);    // 15
    readln(file_otchet, value_buffer);    // 16
    readln(file_otchet, value_buffer);    // 17
    readln(file_otchet, value_buffer);    // 18
    readln(file_otchet, value_buffer);    // 19

    if bHook then
      iSize := StrToInt( Copy( value_buffer, 3, 3))
    else
      iSize := 0;

    readln(file_otchet, value_buffer);    // 20
    readln(file_otchet, value_buffer);    // 21

    if bHook then begin
      iBox := StrToInt(value_buffer);

      a_iCount[iBox, iDiameter] := a_iCount[iBox, iDiameter] + 1;
      a_iCount[iBox, 39] := a_iCount[iBox, 39] + 1;
      a_iCount[19, 39] := a_iCount[19, 39] + 1;
      a_iSize[iBox, iDiameter] := a_iSize[iBox, iDiameter] + iSize;
      a_iSize[iBox, 39] := a_iSize[iBox, 39] + iSize;
      a_iSize[19, 39] := a_iSize[19, 39] + iSize;
      end;

    readln(file_otchet, value_buffer);    // 22
    readln(file_otchet, value_buffer);    // 23
    readln(file_otchet, value_buffer);    // 24
    readln(file_otchet, value_buffer);    // 25
    readln(file_otchet, value_buffer);    // 26

    end;

  for iCounter := 0 to 18 do begin
    bHook := true;

    iCount := a_iCount[iCounter, 39];
    if iCounter = 0 then a_iCount[iCounter, 39] := 0;
    if iCounter = 15 then a_iCount[iCounter, 39] := 0;
    if iCounter = 16 then a_iCount[iCounter, 39] := 0;
    if iCounter = 17 then a_iCount[iCounter, 39] := 0;
    if iCounter = 18 then a_iCount[iCounter, 39] := 0;


    if a_iCount[iCounter, 39] = 0 then begin
      value_buffer := IntToStr(a_iSize[iCounter, 39]);
      if Length( value_buffer ) = 1 then value_buffer := '000' + value_buffer;
      if Length( value_buffer ) = 2 then value_buffer := '00' + value_buffer;
      if Length( value_buffer ) = 3 then value_buffer := '0' + value_buffer;
      Insert(',', value_buffer, length( value_buffer ) - 2);

      write(file_otchet_new, ' ���� �');
      write(file_otchet_new, IntToStr(iCounter) + ':':3 );
      write(file_otchet_new, ' ':15 );
      write(file_otchet_new, IntToStr(iCount) + ' ��':15 );
      writeln(file_otchet_new, value_buffer + ' �3':20 );
      end;

    if a_iCount[iCounter, 39] <> 0 then begin
      for iCounter1 := 0 to 38 do
      if a_iCount[iCounter, iCounter1] <> 0 then begin
        value_buffer := IntToStr(a_iSize[iCounter, iCounter1]);
        if Length( value_buffer ) = 1 then value_buffer := '000' + value_buffer;
        if Length( value_buffer ) = 2 then value_buffer := '00' + value_buffer;
        if Length( value_buffer ) = 3 then value_buffer := '0' + value_buffer;
        Insert(',', value_buffer, length( value_buffer ) - 2);

        if not( bHook ) then begin
          write(file_otchet_new, '       ');
          write(file_otchet_new, ' ':3 );
          write(file_otchet_new, IntToStr(iCounter1) + ' ��':15 );
          write(file_otchet_new, IntToStr(a_iCount[iCounter, iCounter1]) + ' ��':15 );
          writeln(file_otchet_new, value_buffer + ' �3':20 );
          end;

        if bHook then begin
          write(file_otchet_new, ' ���� �');
          write(file_otchet_new, IntToStr(iCounter) + ':':3 );
          write(file_otchet_new, IntToStr(iCounter1) + ' ��':15 );
          write(file_otchet_new, IntToStr(a_iCount[iCounter, iCounter1]) + ' ��':15 );
          writeln(file_otchet_new, value_buffer + ' �3':20 );
          bHook := false;
          end;


        end;
      end;

    end;

  value_buffer := IntToStr(a_iSize[19, 39]);
  if Length( value_buffer ) = 1 then value_buffer := '000' + value_buffer;
  if Length( value_buffer ) = 2 then value_buffer := '00' + value_buffer;
  if Length( value_buffer ) = 3 then value_buffer := '0' + value_buffer;
  Insert(',', value_buffer, length( value_buffer ) - 2);

  writeln(file_otchet_new, '-----------------------------------------------------------------');
  write(file_otchet_new, ' ����� ');
  write(file_otchet_new, ' ':15 );
  write(file_otchet_new, IntToStr(a_iCount[19, 39]) + ' ��':18 );
  writeln(file_otchet_new, value_buffer + ' �3':20 );
  writeln(file_otchet_new, '-----------------------------------------------------------------');

  closefile(file_otchet);
  closefile(file_otchet_new);

  reset(file_otchet_new);

  iCounter := 0;

  with Printer do Begin
    BeginDoc;
    Canvas.Font := Label2.Font;
    while not ( EOF(file_otchet_new)) do begin
      iCounter := iCounter + 1;
      readln(file_otchet_new, value_buffer);

      Canvas.TextOut(100, iCounter*15, value_buffer );

      end;
    EndDoc;
    end;

  closefile(file_otchet_new);

end;

end.
